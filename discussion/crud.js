const http = require("http");

const port = 4000;

// Mock Database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Robert",
		"email": "robert@mail.com"
	}
]

const server = http.createServer(function(req, res) {

	// Route for returning all users upon receiving a GET request
	if(req.url == "/users" && req.method == "GET") {

		// Requests the "/users" path and "GETS" information 
        // Sets the status code to 200, meaning OK
        // Sets response output to JSON data type
		res.writeHead(200, {'Content-Type': 'application/json'});

		// Input HAS to be data type STRING hence the JSON.stringify() method
        // This string input will be converted to desired output data type which is JSON
        // This is done because requests and responses sent between client and a node JS server requires the information to be sent and received as a stringified JSON
		res.write(JSON.stringify(directory));
		res.end()
	}

	if(req.url == "/users" && req.method == "POST") {

		// Declare and intialize a "requestBody" variable to an empty string
		// This will act as a placeholder for the resource/data to be created later on
		let requestBody = "";

		// A stream is a sequence of data
		// Data is received from the client and is processed in the "data" stream
		// The information provided from the request object enters a sequence called "data" the code below will be triggered
	 	// data step - this reads the "data" stream and processes it as the request body
		req.on('data', function(data) {

			// Assigns the data retrieved from the data stream to the requestBody
			requestBody += data;

			
		})

		// end step - only runs after the request has completely been sent
		req.on('end', function() {

			// Check if at this point the requestBody is of data type STRING
	 		// We need this to be of data type JSON to access its properties
			console.log(typeof requestBody);

			// Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			// Create a new object representing the new mock database record
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email 
			}

			// Add the new user into the mock database
			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, {'Content-Type': 'application/json'})
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}
});

server.listen(port);

console.log(`Server is running at localhost:${port}`);
