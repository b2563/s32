
const http = require("http");
http.createServer(function(req, res) {
	// The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
	// The method "GET" means that we will be retrieving or reading information
	if(req.url == "/items" && req.method == "GET") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Data retrieved from the database');
	}
	// The method "POST" means that we will be adding or creating information
	// In this example, we will just be sending a text response for now
	if(req.url == "/items" && req.method == "POST") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Data sent to the database');
	}
}).listen(4000);
console.log(`Server is running at localhost:4000`);